package hellopackage;
import secondpackage.Utilities;
import java.util.Scanner;
import java.util.Random;


public class Greeter{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter integer");
        int number = scan.nextInt();
        System.out.println(Utilities.doubleMe(number));
    }
}
